package com.fantabel.bookmarkkeeper.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {
    @Test
    void testConstructor() {
        Group group = new Group();
        assertNull(group.getName());
    }

    @Test
    void testConstructorComplete() {
        final String GROUP_NAME = "Documentation";
        Group group = new Group(GROUP_NAME);

        assertEquals(GROUP_NAME, group.getName());
    }
}