package com.fantabel.bookmarkkeeper.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class UrlServiceTest {
    private UrlService service;

    @BeforeEach
    void setUp() {
        service = new UrlService();
    }

    @DisplayName("Hello World!")
    @Test
    void testSomething() {
        UrlService service = new UrlService();
        service.createUrl("s");
    }

    @Test
    void testCreateUrl() {
        Url url = service.createUrl("https://www.google.com");
        assertNotNull(url);
    }
}
