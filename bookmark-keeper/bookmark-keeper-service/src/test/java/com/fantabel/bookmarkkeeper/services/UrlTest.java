package com.fantabel.bookmarkkeeper.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class UrlTest {

    @Test
    public void testConstructor() {
        Url url = new Url();
        assertNull(url.getUri());
        assertNull(url.getDescription());
        assertNull(url.getGroup());
    }

    @Test
    public void testConstructorComplete() {
        Url url = new Url("https://www.google.com/", "Google", new Group("Doc"));
        assertEquals("https://www.google.com/", url.getUri());
        assertEquals("Google", url.getDescription());
        assertEquals(new Group("Doc"), url.getGroup());
    }
}
