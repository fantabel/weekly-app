package com.fantabel.bookmarkkeeper.services;

import java.util.Objects;

public class Group {
    private String name;
    public Group() {

    }

    public Group(String group_name) {
        name = group_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return getName().equals(group.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
