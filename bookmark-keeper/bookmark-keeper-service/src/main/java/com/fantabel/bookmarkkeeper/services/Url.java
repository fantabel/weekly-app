package com.fantabel.bookmarkkeeper.services;

public class Url {
    private String uri;
    private String description;
    private Group group;

    public Url() {

    }

    public Url(String uri, String description, Group group) {
        this.uri = uri;
        this.description = description;
        this.group = group;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
