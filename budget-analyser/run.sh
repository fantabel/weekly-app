#!/usr/bin/env bash
DIR="budget-analyser"

java -Dlog4j.configuration=budget-analyser-log4j.properties -cp $DIR/budget-analyser-app/target/budget-analyser-app-1.0.jar:$DIR/budget-analyser-app/target/lib/* com.fantabel.budgetanalyser.app.App
