package com.fantabel.budgetanalyser.service;

import com.fantabel.budgetanalyser.model.dao.BudgetDAO;
import com.fantabel.budgetanalyser.model.entity.BudgetEntry;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BudgetService {
	final Logger logger = LoggerFactory.getLogger(BudgetService.class);
	BudgetDAO budgetDAO = new BudgetDAO();

	public int loadFile(String filepath) {
		logger.debug("Loading file {}.", filepath);
		Path p = Paths.get("./" + filepath);
		try {
			List<String> lignes = Files.readAllLines(p);
			lignes.remove(0);

			for (String s : lignes) {
				BudgetEntry be = stringToBudgetEntry(s);
				budgetDAO.insert(be);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return filepath.length();

	}

	private BudgetEntry stringToBudgetEntry(String s) {
		BudgetEntry be = new BudgetEntry();
		String[] champs = s.split(",");
		be.setBranch(champs[0]);
		be.setAccountNumber(champs[1]);
		be.setDateTransaction(LocalDate.parse(champs[3], DateTimeFormatter.ofPattern("\"yyyy/MM/dd\"")));
		be.setTransactionNumber(champs[4]);
		be.setShortDescription(champs[5]);

		BigDecimal minus = parseAmount(champs[7]);
		BigDecimal plus = parseAmount(champs[8]);
		BigDecimal advance = parseAmount(champs[11]);
		BigDecimal interests = parseAmount(champs[10]);

		be.setAmount(plus.subtract(minus).add(advance).add(interests));
		if (be.getAmount().equals(BigDecimal.ZERO)) {
			logger.debug(Arrays.toString(champs));
		}
		return be;
	}

	private BigDecimal parseAmount(String s) {
		BigDecimal retour = new BigDecimal(0);
		if (!StringUtils.isAllBlank(s.replace("\"", ""))) {
			retour = new BigDecimal(s);
		}
		return retour;
	}
}
