package com.fantabel.budgetanalyser.model.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fantabel.budgetanalyser.model.entity.BudgetEntry;

public class Database {
	final static Logger logger = LoggerFactory.getLogger(Database.class);
	final static String INSERT_TRANSACTION = "insert into transaction (store, amount) values (?, ?)";

	Connection conn;
	PreparedStatement insertTransaction;


	public Database() {
		try {
			conn = getConnection();
			insertTransaction = conn.prepareStatement(INSERT_TRANSACTION);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void init() {
		logger.debug("Initializing database");
	}

	private Connection getConnection () throws SQLException, ClassNotFoundException {
		Class.forName ("org.h2.Driver");
		return DriverManager.getConnection("jdbc:h2:/var/data/budget-analyser-db", "sa", "");

	}

	public void insertTransaction(BudgetEntry be) {
		logger.debug("Inserting " + be.getTransactionNumber() + " of month " + be.getDateTransaction().toString());
		try {
			insertTransaction.setString(1, be.getShortDescription());
			insertTransaction.setBigDecimal(2, be.getAmount());
			insertTransaction.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
