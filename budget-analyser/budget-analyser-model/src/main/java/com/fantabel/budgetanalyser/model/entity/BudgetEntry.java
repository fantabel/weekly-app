package com.fantabel.budgetanalyser.model.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class BudgetEntry {

    private long id;
    private LocalDate dateTransaction;
    private BigDecimal amount;
    private String category;
    private String shortDescription;
    private String description;
    private String branch;
    private String transactionNumber;
    private String accountNumber;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDateTransaction() {
        return dateTransaction;
    }

    public void setDateTransaction(LocalDate dateTransaction) {
        this.dateTransaction = dateTransaction;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionnumber) {
        this.transactionNumber = transactionnumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return "BudgetEntry{" +
                "id=" + id +
                ", dateTransaction=" + dateTransaction +
                ", amount=" + amount +
                ", category='" + category + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", description='" + description + '\'' +
                ", branch='" + branch + '\'' +
                ", transactionNumber='" + transactionNumber + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                '}';
    }
}
