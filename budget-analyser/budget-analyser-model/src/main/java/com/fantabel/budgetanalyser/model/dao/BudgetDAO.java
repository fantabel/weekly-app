package com.fantabel.budgetanalyser.model.dao;

import com.fantabel.budgetanalyser.model.entity.BudgetEntry;
import com.fantabel.budgetanalyser.model.util.Database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BudgetDAO {
	final Logger logger = LoggerFactory.getLogger(BudgetDAO.class);
	private static final String SEPARATOR = ",";
	private Path fileDB;
	Database database;

	public BudgetDAO() {
		database = new Database();
		logger.debug("BudgetDAO object created");

	}

	public void insert(BudgetEntry be) {
		database.insertTransaction(be);
	}
}
