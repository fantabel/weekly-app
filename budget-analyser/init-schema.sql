drop table file;
drop table transaction;
drop table category;

create table file (
    id identity,
    filename varchar(100),
    row_count int,
    year int,
    month int
);

create table category (
	id identity,
	code varchar(4),
	description varchar(100)
);

create table transaction (
	id identity,
	store varchar(255),
	description varchar(500),
	note varchar(1000),
	amount decimal,
	category_id int,
	file_id int,
	foreign key (category_id) references category(id),
	foreign key (file_id) references file(id)
);

