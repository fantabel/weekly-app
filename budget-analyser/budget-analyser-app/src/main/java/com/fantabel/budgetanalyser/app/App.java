package com.fantabel.budgetanalyser.app;

import com.fantabel.budgetanalyser.service.BudgetService;
import com.fantabel.budgetanalyser.service.CategoryService;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Hello world!
 *
 */
public class App {
	final Logger logger = LoggerFactory.getLogger(App.class);
	Options options;
	BudgetService budgetService;
	CategoryService categoryService;

	public App() {
		options = createOptions();
		budgetService = new BudgetService();
		categoryService = new CategoryService();

	}

	public static void main( String[] args )
	{
		App budgetAnalyserApp = new App();
		System.out.println(budgetAnalyserApp.getWelcomeMessage());
		budgetAnalyserApp.process(args);
	}

	public String getWelcomeMessage() {
		return "Welcome to the Budget-Analyser App";
	}

	public void process(String[] args) {
		Optional<CommandLine> cmd = parseOptions(options, args);
		if (!cmd.isPresent()) {
			printHelp();
			System.exit(0);
		}
	}

	public Optional<CommandLine> parseOptions(Options options, String[] args) {
		CommandLineParser parser = new DefaultParser();
		try {
			return Optional.of(parser.parse(options, args));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Optional.empty();
	}

	public Options createOptions() {
		Option help = new Option( "help", "print this message" );

		Options options = new Options();
		options.addOption(help);

		return options;
	}

	public void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "budget-analyser", options );
	}
}
