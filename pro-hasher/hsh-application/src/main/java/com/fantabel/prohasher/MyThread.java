package com.fantabel.prohasher;

import org.springframework.stereotype.Component;

import com.fantabel.prohasher.services.ScryptService;


public class MyThread implements Runnable {
	
	ScryptService service;
	public static final int maxCount = 10;
	private int nbIter;
	
	private String name;
	public MyThread(String name, ScryptService service, int nbIter) {
		this.name = name;
		this.service = service;
		this.nbIter = nbIter;
	}
	
	@Override
	public void run() {
		for (int i = 0 ; i < nbIter ; i ++) {
			long startTime = System.currentTimeMillis();
			String value = service.scrypt("Moo");
			System.out.println(name + "(" + (i + 1) + "/" + nbIter + ") -> " + value + " in " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds");
		}
	}
}
