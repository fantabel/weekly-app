package com.fantabel.prohasher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fantabel.prohasher.services.HashingService;

@SpringBootApplication
public class ProHasherApplication implements CommandLineRunner {
	private static Logger LOG = LoggerFactory.getLogger(ProHasherApplication.class);

	@Autowired
	HashingService hashingService;

	@Override
	public void run(String... strings) throws Exception {
		String value = "password";
		LOG.info("We have boot!");
		String salt = "123";
		/*
		System.out.println(value + " --> md5 : " + hashingService.md5(value));
		System.out.println(value + " --> sha1 : " + hashingService.sha1(value));
		System.out.println(value + " --> sha256 : " + hashingService.sha256(value));
		System.out.println(value + " --> sha512 : " + hashingService.sha512(value));
		System.out.println(value + " --> pbkdf2sha1 : " + hashingService.pbkdf2sha1(value));
		System.out.println(value + " --> pbkdf2sha256 : " + hashingService.pbkdf2sha256(value));
		System.out.println(value + " --> pbkdf2sha512 : " + hashingService.pbkdf2sha512(value));
		System.out.println(value + " --> bcrypt : " + hashingService.bcrypt(value));
		System.out.println(value + " --> scrypt : " + hashingService.scryptHex(value));
		System.out.println(value + " --> scrypt(" + salt + ") : " + hashingService.scryptHex(value, salt));
		System.out.println(value + " --> scryptBase64(" + salt + ") : " + hashingService.scryptBase64(value, salt));
		*/
	}

	public static void main(String[] args) {
		LOG.info("STARTING THE APPLICATION");
		SpringApplication.run(ProHasherApplication.class, args);
		LOG.info("APPLICATION FINISHED");
	}
}
