package com.fantabel.prohasher.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.fantabel.prohasher.MyThread;
import com.fantabel.prohasher.services.ScryptService;

@Component
public class ParallelTester implements CommandLineRunner {
	private static final int maxThread = 10;
	private static final int nbIter = 500;
	@Autowired
	ScryptService service;
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Start parallel job");
		long startTime = System.currentTimeMillis();
		Thread[] threads = new Thread[maxThread];
		for (int i = 0 ; i < maxThread ; i++) {
			threads[i] = new Thread(new MyThread(i + "", service, nbIter / maxThread));
			threads[i].start();
		}
		
		for (Thread t : threads) {
			t.join();
		}
		
		System.out.println(maxThread + " process " + nbIter + " hash in " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds");
	}
}
