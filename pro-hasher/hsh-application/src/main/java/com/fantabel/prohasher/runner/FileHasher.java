package com.fantabel.prohasher.runner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.fantabel.prohasher.services.ScryptService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class FileHasher implements CommandLineRunner {
	private final String COMMA = ",";
	private static int progress = 0;
	private static long startTime;
	private static int costFactor = 1;
	
	@Autowired
	ScryptService service;
	
	@Override
	public void run(String... args) throws Exception {
		List<String[]> inputList = new ArrayList<String[]>();
		
		File inputF = new File("./nas.csv");
		
		InputStream inputFS = new FileInputStream(inputF);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
		//inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
		br.close();

		for (String[] st : inputList) {
			System.out.println(Arrays.toString(st));
		}
	}

	private Function<String, String[]> mapToItem = (line) -> {
		String[] item = new String[7];
		try { 
		startTime = System.currentTimeMillis();
		System.out.println(++costFactor + ": ");
		long cost = (long)Math.pow(2, costFactor);
		double requiredMemory = (128 * cost * service.BLOC_SIZE * service.PARALLELIZATION) / 1024.0 / 1024 / 1024;
		System.out.println(requiredMemory + "g");
		String[] p = line.split(COMMA);
		
		item[0] = p[0];
		item[1] = p[1];
		item[2] = p[2];
		item[3] = service.bytesToBase64(p[2].getBytes());
		item[4] = service.scrypt(p[2], service.COMMON_SALT, (int)cost, service.BLOC_SIZE, service.PARALLELIZATION, service.DESIRED_KEY_LENGTH);
		item[5] = ((System.currentTimeMillis() - startTime) / 1000) + " sec.";
		item[6] = requiredMemory + "g";
		System.out.println(Arrays.toString(item));
		}
		catch (Exception e) {
			System.out.println(" crashed in " + ((System.currentTimeMillis() - startTime) / 1000) + " sec.");
			throw e;
		}
		return item;
	};
}
