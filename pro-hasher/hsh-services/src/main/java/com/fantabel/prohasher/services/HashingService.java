package com.fantabel.prohasher.services;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.bouncycastle.crypto.generators.BCrypt;
import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.common.hash.Hashing;

@Service
public class HashingService {

	private final int COST_FACTOR_BCRYPT = 16;
	private final int COST_FACTOR_SCRYPT = 20;
	private final int BLOC_SIZE = 8;
	private final int PARALLELIZATION = 1;
	private final int DESIRED_KEY_LENGTH = 32;
	private final String DEFAULT_SALT = "123";

	public String md5(String s) {
		/*
		 * MessageDigestPasswordEncoder encoder = new
		 * MessageDigestPasswordEncoder("MD5"); return encoder.encode(s);
		 */

		return Hashing.md5().hashString(s, StandardCharsets.UTF_8).toString();
	}

	public String sha1(String s) {
		return Hashing.sha1().hashString(s, StandardCharsets.UTF_8).toString();
	}

	public String sha256(String s) {
		return Hashing.sha256().hashString(s, StandardCharsets.UTF_8).toString();
	}

	public String sha512(String s) {
		return Hashing.sha512().hashString(s, StandardCharsets.UTF_8).toString();
	}

	public String bytesToHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : bytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	private byte[] getSalt() {
		return DEFAULT_SALT.getBytes();
	}

	public String pbkdf2sha1(String s) {
		byte[] salt = "123".getBytes();
		KeySpec spec = new PBEKeySpec("password".toCharArray(), salt, 1000, 256);
		SecretKeyFactory factory = null;
		byte[] hash = null;
		try {
			factory = SecretKeyFactory
					.getInstance(Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA1.toString());
			hash = factory.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return bytesToHex(hash);

	}

	public String pbkdf2sha256(String s) {
		byte[] salt = "123".getBytes();
		KeySpec spec = new PBEKeySpec("password".toCharArray(), salt, 1000, 256);
		SecretKeyFactory factory = null;
		byte[] hash = null;
		try {
			factory = SecretKeyFactory
					.getInstance(Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA256.toString());
			hash = factory.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();

		}
		return bytesToHex(hash);
	}

	public String pbkdf2sha512(String s) {
		byte[] salt = "123".getBytes();
		KeySpec spec = new PBEKeySpec("password".toCharArray(), salt, 1000, 256);
		SecretKeyFactory factory = null;
		byte[] hash = null;
		try {
			factory = SecretKeyFactory
					.getInstance(Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA512.toString());
			hash = factory.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();

		}
		return bytesToHex(hash);
	}

	public String bcrypt(String s) {
		return bcrypt(s, DEFAULT_SALT);

	}

	public String bcrypt(String s, String salt) {
		byte[] bytes = Arrays.copyOf(salt.getBytes(), 16);
		return bytesToHex(BCrypt.generate(s.getBytes(), bytes, COST_FACTOR_BCRYPT));
	}

	public String scryptHex(String s) {
		return scryptHex(s, DEFAULT_SALT);
	}

	public String scryptHex(String s, String salt) {
		return bytesToHex(scrypt(s, salt));
	}

	public String scryptHex(String s, String salt, int N, int r, int p, int keyLen) {
		return bytesToHex(scrypt(s, salt, N, r, p, keyLen));
	}

	public String scryptBase64(String s) {
		return scryptBase64(s, DEFAULT_SALT);
	}

	public String scryptBase64(String s, String salt) {
		return Base64.toBase64String(scrypt(s, salt));
	}

	public String scryptBase64(String s, String salt, int N, int r, int p, int keyLen) {
		return Base64.toBase64String(scrypt(s, salt, N, r, p, keyLen));
	}

	public byte[] scrypt(String s) {
		return scrypt(s, DEFAULT_SALT);
	}

	/**
	 * Scrypt documentation
	 * <ul>
	 * <li>P - the bytes of the pass phrase.</li>
	 * <li>S - the salt to use for this invocation.
	 * <li>N - CPU/Memory cost parameter. Must be larger than 1, a power of 2 and
	 * less than 2^(128 * r / 8).</li>
	 * <li>r - the block size, must be >= 1.</li>
	 * <li>p - Parallelization parameter. Must be a positive integer less than or
	 * equal to Integer.MAX_VALUE / (128 * r * 8). dkLen - the length of the key to
	 * generate.</li>
	 * </ul>
	 * 
	 * @param s
	 * @param salt
	 * @return
	 */

	public byte[] scrypt(String s, String salt) {
		return scrypt(s, salt, (int) Math.pow(2, COST_FACTOR_SCRYPT), BLOC_SIZE, PARALLELIZATION, DESIRED_KEY_LENGTH);
	}

	public byte[] scrypt(String s, String salt, int N, int r, int p, int keyLen) {
		return SCrypt.generate(s.getBytes(StandardCharsets.UTF_8), salt.getBytes(StandardCharsets.UTF_8), N, r, p,
				keyLen);
	}
}
