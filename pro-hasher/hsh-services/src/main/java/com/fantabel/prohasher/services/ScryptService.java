package com.fantabel.prohasher.services;

import java.nio.charset.StandardCharsets;

import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.stereotype.Service;

@Service
public class ScryptService {
	public final int COST_FACTOR_SCRYPT = 19;
	public final int BLOC_SIZE = 8;
	public final int PARALLELIZATION = 1;
	public final int DESIRED_KEY_LENGTH = 32;
	public final String COMMON_SALT = "n=xDS5.dty]aPE2@YB>c-4}z*g!N?X/9";

	public String bytesToHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : bytes) {
			sb.append(String.format("%02x ", b));
		}
		return sb.toString().replaceAll("(.{48})", "$1\n");
	}
	
	public String bytesToBase64(byte[] bytes) {
		return Base64.toBase64String(bytes);
	}
	
	public String scrypt(String s) {
		return scrypt(s, COMMON_SALT);
	}
	
	public String scrypt(String s, String salt) {
		return scrypt(s, COMMON_SALT, (int) Math.pow(2, COST_FACTOR_SCRYPT), BLOC_SIZE, PARALLELIZATION, DESIRED_KEY_LENGTH);
	}
	
	public String scrypt(String s, String salt, int N, int r, int p, int keyLen) {
		return bytesToBase64(scryptBytes(s, salt, N, r, p,	keyLen));
	}
	
	private byte[] scryptBytes(String s, String salt, int N, int r, int p, int keyLen) {
		return SCrypt.generate(s.getBytes(StandardCharsets.UTF_8), salt.getBytes(StandardCharsets.UTF_8), N, r, p,
				keyLen);
	}
}
