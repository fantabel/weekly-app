CREATE TABLE users (
	UserId INTEGER NOT NULL AUTO_INCREMENT,
	Name VARCHAR(100) NOT NULL,
	Sex VARCHAR(1) NOT NULL,
	Height INTEGER NOT NULL,
	Birthdate DATE NOT NULL,
	CONSTRAINT UserPk PRIMARY KEY (UserId)
);

INSERT INTO users ( Name, Sex, Height, Birthdate ) VALUES ( 'Jimmy St-Hilaire', 'M', 192, '1983-10-14' );
INSERT INTO users ( Name, Sex, Height, Birthdate ) VALUES ( 'Marie-Élaine Lajoie', 'F', 157, '1983-12-20' );
INSERT INTO users ( Name, Sex, Height, Birthdate ) VALUES ( 'Rose St-Hilaire', 'F', 0, '2013-12-21' );
INSERT INTO users ( Name, Sex, Height, Birthdate ) VALUES ( 'Meghan St-Hilaire', 'F', 0, '2015-01-08' );
INSERT INTO users ( Name, Sex, Height, Birthdate ) VALUES ( 'Eliam St-Hilaire', 'M', 0, '2017-03-07' );
