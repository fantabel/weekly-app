CREATE TABLE weightentries (
	WeightEntryId INTEGER NOT NULL AUTO_INCREMENT,
	Day DATE NOT NULL,
	WeightInKilos DECIMAL(5, 2) NOT NULL,
	UserId INTEGER NOT NULL,

	CONSTRAINT WeightEntryPk
		PRIMARY KEY (WeightEntryId),
	
	CONSTRAINT WeightEntryUserFk
		FOREIGN KEY ( UserId )
			REFERENCES users(UserId)
)
