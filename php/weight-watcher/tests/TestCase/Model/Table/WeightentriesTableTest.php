<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WeightentriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WeightentriesTable Test Case
 */
class WeightentriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WeightentriesTable
     */
    protected $Weightentries;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Weightentries',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Weightentries') ? [] : ['className' => WeightentriesTable::class];
        $this->Weightentries = TableRegistry::getTableLocator()->get('Weightentries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Weightentries);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
