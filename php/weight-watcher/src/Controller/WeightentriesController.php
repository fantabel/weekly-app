<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Weightentries Controller
 *
 * @property \App\Model\Table\WeightentriesTable $Weightentries
 * @method \App\Model\Entity\Weightentry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WeightentriesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $weightentries = $this->paginate($this->Weightentries);

        $this->set(compact('weightentries'));
    }

    /**
     * View method
     *
     * @param string|null $id Weightentry id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $weightentry = $this->Weightentries->get($id, [
            'contain' => [],
        ]);

        $this->set('weightentry', $weightentry);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $weightentry = $this->Weightentries->newEmptyEntity();
        if ($this->request->is('post')) {
            $weightentry = $this->Weightentries->patchEntity($weightentry, $this->request->getData());
            if ($this->Weightentries->save($weightentry)) {
                $this->Flash->success(__('The weightentry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The weightentry could not be saved. Please, try again.'));
        }
        $this->set(compact('weightentry'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Weightentry id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $weightentry = $this->Weightentries->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weightentry = $this->Weightentries->patchEntity($weightentry, $this->request->getData());
            if ($this->Weightentries->save($weightentry)) {
                $this->Flash->success(__('The weightentry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The weightentry could not be saved. Please, try again.'));
        }
        $this->set(compact('weightentry'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Weightentry id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $weightentry = $this->Weightentries->get($id);
        if ($this->Weightentries->delete($weightentry)) {
            $this->Flash->success(__('The weightentry has been deleted.'));
        } else {
            $this->Flash->error(__('The weightentry could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
