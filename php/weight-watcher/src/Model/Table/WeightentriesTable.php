<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Weightentries Model
 *
 * @method \App\Model\Entity\Weightentry newEmptyEntity()
 * @method \App\Model\Entity\Weightentry newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Weightentry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Weightentry get($primaryKey, $options = [])
 * @method \App\Model\Entity\Weightentry findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Weightentry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Weightentry[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Weightentry|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Weightentry saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Weightentry[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Weightentry[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Weightentry[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Weightentry[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class WeightentriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('weightentries');
        $this->setDisplayField('WeightEntryId');
        $this->setPrimaryKey('WeightEntryId');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('WeightEntryId')
            ->allowEmptyString('WeightEntryId', null, 'create');

        $validator
            ->date('Day')
            ->requirePresence('Day', 'create')
            ->notEmptyDate('Day');

        $validator
            ->decimal('WeightInKilos')
            ->requirePresence('WeightInKilos', 'create')
            ->notEmptyString('WeightInKilos');

        $validator
            ->integer('UserId')
            ->requirePresence('UserId', 'create')
            ->notEmptyString('UserId');

        return $validator;
    }
}
