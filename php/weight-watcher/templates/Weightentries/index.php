<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Weightentry[]|\Cake\Collection\CollectionInterface $weightentries
 */
?>
<div class="weightentries index content">
    <?= $this->Html->link(__('New Weightentry'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Weightentries') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('WeightEntryId') ?></th>
                    <th><?= $this->Paginator->sort('Day') ?></th>
                    <th><?= $this->Paginator->sort('WeightInKilos') ?></th>
                    <th><?= $this->Paginator->sort('UserId') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($weightentries as $weightentry): ?>
                <tr>
                    <td><?= $this->Number->format($weightentry->WeightEntryId) ?></td>
                    <td><?= h($weightentry->Day) ?></td>
                    <td><?= $this->Number->format($weightentry->WeightInKilos) ?></td>
                    <td><?= $this->Number->format($weightentry->UserId) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $weightentry->WeightEntryId]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $weightentry->WeightEntryId]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $weightentry->WeightEntryId], ['confirm' => __('Are you sure you want to delete # {0}?', $weightentry->WeightEntryId)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
