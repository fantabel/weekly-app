<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Weightentry $weightentry
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Weightentries'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="weightentries form content">
            <?= $this->Form->create($weightentry) ?>
            <fieldset>
                <legend><?= __('Add Weightentry') ?></legend>
                <?php
                    echo $this->Form->control('Day');
                    echo $this->Form->control('WeightInKilos');
                    echo $this->Form->control('UserId');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
