<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Weightentry $weightentry
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Weightentry'), ['action' => 'edit', $weightentry->WeightEntryId], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Weightentry'), ['action' => 'delete', $weightentry->WeightEntryId], ['confirm' => __('Are you sure you want to delete # {0}?', $weightentry->WeightEntryId), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Weightentries'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Weightentry'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="weightentries view content">
            <h3><?= h($weightentry->WeightEntryId) ?></h3>
            <table>
                <tr>
                    <th><?= __('WeightEntryId') ?></th>
                    <td><?= $this->Number->format($weightentry->WeightEntryId) ?></td>
                </tr>
                <tr>
                    <th><?= __('WeightInKilos') ?></th>
                    <td><?= $this->Number->format($weightentry->WeightInKilos) ?></td>
                </tr>
                <tr>
                    <th><?= __('UserId') ?></th>
                    <td><?= $this->Number->format($weightentry->UserId) ?></td>
                </tr>
                <tr>
                    <th><?= __('Day') ?></th>
                    <td><?= h($weightentry->Day) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
