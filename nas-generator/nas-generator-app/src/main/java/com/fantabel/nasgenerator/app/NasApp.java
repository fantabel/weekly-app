package com.fantabel.nasgenerator.app;

import com.fantabel.nasgenerator.service.NasService;

public class NasApp {
    public static void main(String[] argv) {
        long num = 200;
        long start = 2;

        if (argv.length > 0)
            num = Long.valueOf(argv[0]);

        if (argv.length > 1)
            start = Long.valueOf(argv[1]);

        NasService service = new NasService();
        String nas = service.generateNextValidNas(start * 100000000L);

        for (int i = 1; i <= num; i++) {
            nas = service.generateNextValidNas(nas);

            System.out.print(nas + " ");
            if (i % 20 == 0)
                System.out.println("");
        }
    }
}
