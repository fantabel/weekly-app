package com.fantabel.nasgenerator.service;

import org.apache.commons.lang3.StringUtils;

public class NasService {

    public String generateRandomNas() {
        Long randomNumber = (long) (Math.random() * 1_000_000_000);
        if (validerNas(randomNumber)) {
            return convertNas(randomNumber);
        }
        return generateRandomNas();
    }

    public boolean validerNas(String nas) {
        if (!StringUtils.isNumericSpace(nas))
            return false;
        if (nas.length() != 9)
            return false;

        int multiplicator = 1;
        int sum = 0;
        for (char c : nas.toCharArray()) {

            int n = Character.getNumericValue(c);
            int tempSum = n * multiplicator;
            while (tempSum > 0) {
                sum += tempSum % 10;
                tempSum /= 10;
            }
            multiplicator = multiplicator == 1 ? 2 : 1;

        }

        return (sum % 10) == 0;
    }

    private boolean validerNas(Long number) {
        return validerNas(convertNas(number));
    }

    public String generateNextValidNas(String nas) {
        return generateNextValidNas(Long.valueOf(nas));
    }

    public String generateNextValidNas(Long number) {
        do {
            number++;
            if (number > 999999999L)
                throw new IllegalArgumentException("Aucun nas suivant");
        } while (!validerNas(convertNas(number)));
        return convertNas(number);
    }

    public String convertNas(Long number) {
        String s = String.format("%09d", number);

        return s.substring(s.length() - 9);
    }
}
