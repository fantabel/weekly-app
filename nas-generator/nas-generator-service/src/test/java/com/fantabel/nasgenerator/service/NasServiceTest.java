package com.fantabel.nasgenerator.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

class NasServiceTest {
    NasService service;
    static Function<String, Boolean> validNas;

    @BeforeAll
    static void setupClass() {
        validNas = (s) -> {
            if (!StringUtils.isNumericSpace(s))
                return false;
            if (s.length() != 9)
                return false;

            int multiplicator = 1;
            int sum = 0;
            for (char c : s.toCharArray()) {

                int n = Character.getNumericValue(c);
                int tempSum = n * multiplicator;
                while (tempSum > 0) {
                    sum += tempSum % 10;
                    tempSum /= 10;
                }
                multiplicator = multiplicator == 1 ? 2 : 1;

            }

            return (sum % 10) == 0;
        };
    }

    @BeforeEach
    void setUp() {
        service = new NasService();
    }

    @RepeatedTest(100)
    void testNasFormat() {
        // Given

        // When
        String nas = service.generateRandomNas();

        // Then
        assertNotNull(nas);
        assertEquals(9, nas.length(), "For nas:" + nas);
        assertTrue(true);

    }

    @RepeatedTest(100)
    void testNasAlwaysDifferent() {
        // Given
        String nas1;
        String nas2;

        // When
        nas1 = service.generateRandomNas();
        nas2 = service.generateRandomNas();

        // Then
        assertNotEquals(nas1, nas2);
    }

    @RepeatedTest(100)
    void testNasIsValid() {
        // Given

        // When
        String nas = service.generateRandomNas();

        // Then
        assertTrue(validNas.apply(nas), nas + " is not valid.");

    }

    @RepeatedTest(100)
    void testGenerateNextValidNas() {
        // Given
        Long anyRandomNumber = (long) (Math.random() * 1_000_000_000);

        // When
        String nextNas = service.generateNextValidNas(anyRandomNumber);

        // Then
        assertTrue(validNas.apply(nextNas), nextNas + " is not valid.");
    }

    @RepeatedTest(100)
    void testGenerateNextValidNasString() {
        // Given
        String nas = service.generateRandomNas();

        // When
        String nextNas = service.generateNextValidNas(nas);

        // Then
        assertTrue(validNas.apply(nextNas), nextNas + " is not valid.");
        assertThrows(IllegalArgumentException.class, () -> service.generateNextValidNas("A"));
    }

    @Test
    void testLastNasGenerateNext() {
        // Given
        String nas = "999999999";

        // When

        // Then
        assertThrows(IllegalArgumentException.class, () -> service.generateNextValidNas(nas));
    }

    @ParameterizedTest
    @MethodSource("listOfNumberAndNas")
    void testLongToNas(Long number, String s) {
        // Given

        // When

        // Then
        assertEquals(s, service.convertNas(number));

    }

    static Stream<? extends Arguments> listOfNumberAndNas() {
        return Stream.of(
                Arguments.of(new Long(0), "000000000"),
                Arguments.of(new Long(999999999), "999999999"),
                Arguments.of(new Long(123456789), "123456789"),
                Arguments.of(new Long(123), "000000123"),
                Arguments.of(new Long(12345678987654321L), "987654321")
        );
    }
}
