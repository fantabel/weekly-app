package com.fantabel.weightracker.model.entity;

import java.time.LocalDate;

public class WeightEntry {
	
	Long id;
	LocalDate date;
	double weight;
	
	public WeightEntry() {
		this(0);
	}
	
	public WeightEntry(double weight) {
		this(LocalDate.now(), weight);
	}
	
	public WeightEntry(LocalDate time, double weight) {
		setDate(time);
		setWeight(weight);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public String toString() {
		return getId() + "\t" + getDate() + "\t" + getWeight();
		
	}

}
