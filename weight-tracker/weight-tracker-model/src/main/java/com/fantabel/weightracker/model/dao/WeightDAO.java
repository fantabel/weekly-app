package com.fantabel.weightracker.model.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.fantabel.weightracker.model.entity.WeightEntry;

public class WeightDAO {

	private final static String FILE_NAME = "weightEntries.dat";
	private final static String PATH = ".";
	private final static String SEPARATOR = "\t";
	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private final static int ID_POSITION = 0;
	private final static int DATE_POSITION = 1;
	private final static int WEIGHT_POSITION = 2;
	private Path fileDB;
	List<WeightEntry> database;

	public WeightDAO() {
		fileDB = Paths.get(PATH + "/" + FILE_NAME);
		loadEntries();
	}

	public List<WeightEntry> getAllEntries() {
		// TODO clone the database (deep-copy)
		return database;
	}

	private void saveEntries() {
		/*
		 * try (BufferedWriter writer = Files.newBufferedWriter(fileDB,
		 * Charset.forName("UTF-8"))) { for (WeightEntry entry : database) {
		 * writer.write(entry.toString()); } } catch (IOException ex) {
		 * ex.printStackTrace(); }
		 */

		StringBuilder sb = new StringBuilder();
		for (WeightEntry entry : database) {
			sb.append(entry.toString() + "\n");
		}
		try {
			Files.write(fileDB, sb.toString().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void add(WeightEntry entry) {
		if (entry.getId() == null) {
			entry.setId(getNextId());
		}
		database.add(entry);
		saveEntries();
		reloadEntries();
	}
	
	private long getNextId() {
		return database.stream().max(Comparator.comparing(WeightEntry::getId)).get().getId() + 1;
	}

	private void reloadEntries() {
		loadEntries();
	}

	private void loadEntries() {

		database = new ArrayList<>();
		if (!Files.exists(fileDB)) {
			try {
				Files.createFile(fileDB);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			List<String> content = Files.readAllLines(fileDB);
			for (String s : content) {
				WeightEntry temp = new WeightEntry();
				String[] tokens = s.split(SEPARATOR);
				temp.setId(Long.parseLong(tokens[ID_POSITION]));
				temp.setDate(LocalDate.parse(tokens[DATE_POSITION], formatter));
				temp.setWeight(Double.parseDouble(tokens[WEIGHT_POSITION]));
				database.add(temp);

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
