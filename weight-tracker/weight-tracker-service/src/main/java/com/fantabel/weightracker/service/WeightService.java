 package com.fantabel.weightracker.service;

import java.time.LocalDate;
import java.util.List;

import com.fantabel.weightracker.model.dao.WeightDAO;
import com.fantabel.weightracker.model.entity.WeightEntry;

public class WeightService {
	
	WeightDAO weightDAO;
	
	public WeightService() {
		weightDAO = new WeightDAO();
	}
	
	public void logWeight(double weight) {
		logWeight(LocalDate.now(), weight);
	}
	
	public void logWeight(LocalDate date, double weight) {
		logWeight(new WeightEntry(date, weight));
	}
	
	public void logWeight(WeightEntry weightEntry) {
		weightDAO.add(weightEntry);
	}
	
	public void editEntry(long id, double weight) {
		
	}
	
	public void editEntry(long id, LocalDate date) {
		
	}
	
	public void editEntry(long id, LocalDate date, double weight) {
		
	}
	
	public void editEntry(WeightEntry weightEntry) {
		
	}
	
	public List<WeightEntry> listEntries() {
		return weightDAO.getAllEntries();
	}
	
	public WeightEntry getEntry(int number) {
		return null;
		
	}
	
	public List<WeightEntry> listEntries(LocalDate upTo) {
		return null;
		
	}

}
