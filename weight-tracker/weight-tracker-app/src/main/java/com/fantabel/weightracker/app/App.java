package com.fantabel.weightracker.app;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.fantabel.weightracker.model.entity.WeightEntry;
import com.fantabel.weightracker.service.WeightService;

//import util.Util;

public class App {

	Options options;
	WeightService weightService;
	
	public App() {
		options = createOptions();
		weightService = new WeightService();
	}
	
	public String getWelcomeMessage() {
		return "Welcome to the Weight Tracker App";
	}
	
    public static void main(String[] args) {
    	App weightTrackerApp = new App();
        System.out.println(weightTrackerApp.getWelcomeMessage());
        
        //weightTrackerApp.printHelp();
        weightTrackerApp.process(args);
    }
    
    public void process(String[] args) {
    	Optional<CommandLine> cmd = parseOptions(options, args);
    	if (!cmd.isPresent()) {
    		printHelp();
    		System.exit(0);
    	}
    	List<WeightEntry> list = weightService.listEntries();
    	weightService.logWeight(LocalDate.now(), (Math.random() * 200));
    	for (WeightEntry we : list) {
    		System.out.println(we.toString());
    	}
    }
    
    public Optional<CommandLine> parseOptions(Options options, String[] args) {
    	CommandLineParser parser = new DefaultParser();
    	try {
			return Optional.of(parser.parse(options, args));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Optional.empty();
    }

    public Options createOptions() {
    	Option help = new Option( "help", "print this message" );
    	
    	Options options = new Options();
    	options.addOption(help);
    	
	    return options;
    }
    
    public void printHelp() {
    	HelpFormatter formatter = new HelpFormatter();
    	formatter.printHelp( "weightracker", options );
    }
}
